const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];

// 1. Get all items that are available :
const allItems = items.filter((obj) => {
    if (obj.available === true) {
        return items;
    }
});
console.log(allItems);

//2.Get all items containing only Vitamin C.
const vitaminC = items.filter((obj) => {
    if (obj.contains === "Vitamin C") {
        return obj;
    }
});
console.log(vitaminC);

//3.Get all items containing Vitamin A.
const vitaminA = items.filter((obj) => {
    if (obj.contains.includes("Vitamin A")) {
        return items;
    }
});
console.log(vitaminA);

/* 4.Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.*/     
let item = [];
const vitamins = items.reduce((acc, curr) => {
    item = curr.contains.split(",");
    item.map((vitamin) => {
        if (curr.contains.includes(vitamin) && acc[vitamin] === undefined) {
            acc[vitamin] = [curr.name];
        }
        else {
            if (curr.contains.includes(vitamin)) {
                acc.add(curr.name);
            }
        }

    })
    return acc;
}, {});
console.log(vitamins);
// vitamins.reduce((acc,curr) =>{
//     if (acc[curr] === undefined){
//         acc[curr] = Object.key(curr)
//     }
//     return acc;
// },{});

