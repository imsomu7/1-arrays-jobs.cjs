const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}
// Q1. Find all the movies with total earnings more than $500M.

const movies$500 = Object.entries(favouritesMovies).filter((obj) => {
    //let array = obj[1].totalEarnings;
    //console.log(array);
    if (obj[1].totalEarnings > "$500M") {
        return obj;
    }
});
//console.log(movies$500);

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
const fourOscarNominatedMovies = Object.entries(favouritesMovies).filter((movies) => {
    if (movies[1].oscarNominations > "3" || movies[1].totalEarnings > "$500M") {
        return movies;
    }
});
//console.log(fourOscarNominatedMovies);

// Q.3 Find all movies of the actor "Leonardo Dicaprio".
const Leonardo = Object.entries(favouritesMovies).filter((movies) => {
    if (movies[1].actors.includes("Leonardo Dicaprio")) {
        return movies;
    }

});
//console.log(Leonardo);

/*Q.4 Sort movies (based on IMDB rating)
if IMDB ratings are same, compare totalEarning as the secondary metric.*/
const highestImdbRating = Object.entries(favouritesMovies).sort(([,mov1],[,mov2]) => {
    if (mov1.imdbRating < mov2.imdbRating) {
        return 1;
    }
    else if (mov1.imdbRating === mov2.imdbRating) {
        if (parseInt(mov1.totalEarnings.substring(1)) < parseInt(mov2.totalEarnings.substring(1))) {
            return 1;
        }
        else {
            return -1;
        }
    }
    else {
        return -1;
    }
});
console.log(highestImdbRating);

/* Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime */
